## Git Cheatsheet

Link to the [reference 1](https://www.freecodecamp.org/news/git-cheat-sheet/) and [reference 2](https://about.gitlab.com/images/press/git-cheat-sheet.pdf).

### Most Helpful Commands

#### Basic Commands

| Command | Usage |
|---------|-------|
| `git init` | Initialize a new Git repository |
| `git add .` | Add all modified files to the staging area |
| `git commit -m "Message"` | Commit the staged changes with a descriptive message |
| `git status` | Show the current status of the working directory and staging area |
| `git log` | Show the commit history |
| `git branch` | List all branches in the repository |
| `git checkout <branch>` | Switch to a different branch |
| `git merge <branch>` | Merge changes from one branch into another |
| `git pull` | Fetch and merge changes from a remote repository |
| `git push` | Push local commits to a remote repository |

#### Config & Setup
`
| Command | Usage |
|---------|-------|
| `git config -l` | Return a list of information about the git configuration including username and email |
| `git config --global user.name "rochdikhalid"`| Setup Git username |
| `git config --global user.email "contact@rochdikhalid.com"` | Setup Git user email |
| `git config --g`obal credential.helper cache` | Store login credentials in the cache to avoid have typing them in each time |

#### Staging

| Command | Usage |
|---------|-------|
| `git add filename_here` | Add a file to the staging area |
| `git add fil*` | Add only certain files to the staging area |
| `git add -p <filename>` | Ask which changes made in a file needs to be staged |

