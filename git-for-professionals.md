# Git for Professionals

Link to the [reference](https://www.youtube.com/watch?v=Uszj_k0DGsg) course.

## The perfect Commit

1. Add the **right** changes.
	* Combine the changes of the same topic into one single commit.
	* Use the command `git diff <file>` to see the changes of a specific file.
	* The command `git add -p <file>` allows adding a **hunk** to the staging area. A hunk refers to a section of changes within a file.
2. Compose a **good** commit message.
	* Use the command `git commit` to write the subject and the body (add an empty after the subject so that can recognize this as the body).
	* Subject = concise summary of what happened.
	* Body = more detailed explanation.
		* What is now different than before?
		* What is the reason for the change?
		* Is there anything to watch out for / anything particularly remarkable?
	* Use the command `git log` to display the list of commits made in a repository and ensure your commits are made as expected.

## Branching Strategies

### Model 1: Mainline Development 

"Always Be Integrating"

* Few branches.
* Relatively small branches.
* High-quality testing & QA standards.

### Model 2: State, Release, and Feature Branches

"Branches Enhance Structures & Workflow"

* Different types of branches.
* Fullfill different types of jobs.

### Long-running Branches

* Exist through the complete lifetime of the project.
* Mirror stages in the development lifecycle.
* Common convention: no direct commits (ex: don't add non-reviewed code to the main branch).

Ex: develop, staging, deploy, etc.

### Short-lived Branches

* Created for new features, bug fixes, refactorings, experiments, etc.
* Short-lived branch is mainly based on a main branch.
* Deleted after integration (merge/rebase).

### Two Example Branching Strategies

#### GitHub Flow

* Very simple, very lean: only one long-running branch with feature branches.

#### GitFlow

* More structure, more rules.
* Long-running: 'main' + 'develop'.
* Short-lived: features, releases, hotfixes.

## Pull Requests

"Communicating about and Reviewing Code"

* A Pull Request is based on branches not commits.
* A Pull Request invites reviewers to provide feedback before merging.
	1. Contributing code to other repositories that you don't have access to.
    2. Creating a **Fork** of the original repository, where changes are made.
    3. Suggest those changes to be included via a Pull Request.

A new git command to push a local branch to a remote repository:

```bash
git push --set-upstream origin <branch_name>
```

## Merge Conflicts

"Nobody Likes Them"

1. When they might occur?

A merge conflict occurs when two or more branches have made changes to the same part of a file, and these changes are incompatible with each other.


2. How to solve them?

* The version control system cannot automatically determine which changes to keep, as the changes overlap. 
* It marks the conflicted areas in the file(s) and prompts the developer to manually resolve the conflicts by choosing which changes to keep, modifying the code accordingly, or incorporating both sets of changes if applicable.

3. How to undo a conflict and start over?

"You can always undo and start fresh"

```bash
git merge --abort
git rebase --abort
```

## Merge vs Rebase

Merge and rebase are two different strategies used in version control systems like Git to integrate changes from one branch into another.

#### Merge

The merge strategy integrates changes from one branch into another by creating a new merge commit.

* **Integration Strategy**: Combines the changes from one branch into another branch.

* **Commit History**: Preserves the commit history of both the source and target branches.

* **Merge Commits**: Creates a new commit (merge commit) that represents the combination of changes from both branches.

* **Visibility**: Clearly shows the branching history and the points where branches were merged.

##### Advantages

* Preserves the original commit history of both branches.
* Allows for a straightforward integration of changes from multiple contributors or parallel development efforts.

##### Disadvantages

* Can result in a cluttered commit history with frequent merge commits.
* May lead to a less linear history, making it harder to trace the development timeline.*

#### Rebase

When you rebase a branch onto another branch, each commit from the rebased branch is applied individually onto the tip of the target branch. This process effectively replays the commits one by one on top of the target branch, creating a new history of changes.

Each commit from the rebased branch becomes part of the target branch's history, and as each commit is integrated, it appears as if it was developed directly on top of the current state of the target branch. This sequential integration of commits results in a linear history, where each commit follows the previous one without any branching or merging.

* **Integration Strategy**: Applies the commits from one branch onto another branch by reapplying each commit individually.

* **Commit History**: Rewrites the commit history of the target branch, placing the commits from the rebased branch on top.

* **Merge Commits**: Does not create merge commits by default, resulting in a linear history.
Visibility: Offers a cleaner, linear history without merge commits.

##### Advantages

* Results in a cleaner commit history, with a linear sequence of commits.
* Helps maintain a more readable and understandable project history.

##### Disadvantages

* Rewrites the commit history, which can cause complications if the rebased branch has already been shared with others.
* Can lead to potential conflicts when rebasing if changes in the target branch conflict with those in the rebased branch.


Don't use Rebase on commits that you've already pushed/shared on a remote repository. Instead, use it for cleaning up your local commit history before merging it into a shared team branch.